import os, requests, threading, datetime, random
from pyvirtualdisplay import Display
from selenium import webdriver
from time import sleep

def send_start(worker_name):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/beacon/', data={'worker': worker_name}, timeout=5)
    except:
        print('err sent_beacon()')

def send_finish(worker_name, th, ah):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/', data={'worker': worker_name, 'th':th, 'ah':ah}, timeout=5)
    except:
        print('err send_finish()')

def main():
    with open('id', 'r') as f:
        worker_name = f.read()
    with open('node', 'r') as f:
        node = f.read()
    send_start(worker_name)
    display = Display(visible=0, size=(1366, 768))
    display.start()
    driver = webdriver.Firefox()
    driver.get(node)
    sleep(60*random.randint(5,20))
    th = 0#driver.find_element_by_id('th').text
    ah = 0#driver.find_element_by_id('ah').text
    driver.close()
    display.stop()
    send_finish(worker_name, th, ah)

if __name__ == '__main__':
    main()
#2018-02-27 09:36:02.109531
#2018-02-27 09:56:01.369529
#2018-02-27 10:21:01.466502
#2018-02-27 10:48:01.213634
#2018-02-27 11:07:01.835524
#2018-02-27 11:31:01.568014
#2018-02-27 11:49:01.729431
#2018-02-27 12:10:01.223558
#2018-02-27 12:40:01.241802
#2018-02-27 13:03:01.636880
#2018-02-27 13:26:01.687590
#2018-02-27 13:43:01.739971
#2018-02-27 13:58:01.494788
#2018-02-27 14:27:02.143564
#2018-02-27 14:41:01.723590
#2018-02-27 15:05:01.288354
#2018-02-27 15:31:01.823129
#2018-02-27 15:53:01.546182
#2018-02-27 16:21:01.647412
#2018-02-27 17:33:01.454112
#2018-02-27 18:18:01.695226
#2018-02-27 19:16:01.392824
#2018-02-27 20:31:01.330410